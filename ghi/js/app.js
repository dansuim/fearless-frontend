// function createCard(nameTag, descriptionTag, imageTag) {
function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${new Date(starts).toLocaleDateString()} -
        ${new Date(ends).toLocaleDateString()}
        </div>
    `;
    }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        // console.error("There was an error");
        const container = document.querySelector(".container");
        container.innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert">' + 'Something went wrong trying to get the data' + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

      } else {
        const data = await response.json();
        // console.log("log data", data)
            // for (let conference of data.conferences) {
            //     const conference = data.conferences[0];
            //     const nameTag = document.querySelector('.card-title');
            //     nameTag.innerHTML = conference.name;

            //     const detailUrl = `http://localhost:8000${conference.href}`;
            //     const detailResponse = await fetch(detailUrl);
            //     if (detailResponse.ok) {
            //         const details = await detailResponse.json();

            //         const description = details.conference.description;
            //         const descriptionTag = document.querySelector('.card-text');
            //         descriptionTag.innerHTML = description;


            //         const imageTag = document.querySelector('.card-img-top');
            //         //imageTag.src = "hello"
            //         imageTag.src = details.conference.location.picture_url;
            //         const html = createCard(nameTag, descriptionTag, imageTag);
            //         //console.log(html);
            //     }
            // }
            let index = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details)
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const location = details.conference.location.name;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    //   console.log(html);
                    const column = document.querySelector(`#col-${index % 3}`);

                    column.innerHTML += html;
                    index += 1;
                }
            }
        }
    } catch (e) {
      // Figure out what to do if an error is raised
    //   console.error("There was an error", e);
      const container = document.querySelector(".container");
      container.innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert">' + 'Are you sure you are looking in the right place?' + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

    }
  });
