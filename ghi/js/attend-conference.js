window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
        }

    // Here, add the 'd-none' class to the loading icon
    // const addLoadingIcon = document.getElementById('loading-icon');
    const addLoadingIcon = document.getElementById('loading-conference-spinner');
    addLoadingIcon.classList.add('d-none');


    // Here, remove the 'd-none' class from the select tag
    const removeLoadingIcon = document.getElementById('conference');
    removeLoadingIcon.classList.remove('d-none');

    // Get the attendee form element by its id
    const formTag = document.getElementById('create-attendee-form');
    //create event listener
    formTag.addEventListener('submit', async event => {
        // Prevent the default from happening
        event.preventDefault();
        // console.log('need to submit the form data');
        // Create a FormData object from the form
        const formData = new FormData(formTag);
        // Get a new object from the form data's entries
        const json = JSON.stringify(Object.fromEntries(formData));
        // console.log(json);
        try {
            // Create options for the fetch
            // Make the fetch using the await keyword to the URL http://localhost:8001/api/attendees/
            const attendeeUrl = 'http://localhost:8001/api/attendees/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(attendeeUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newAttendee = await response.json();
                console.log(newAttendee);

                // Here, add the 'd-none' class to the loading icon
                // const addLoadingIcon = document.getElementById('loading-icon');
                const successAlert = document.getElementById('success-message');
                successAlert.classList.remove('d-none');


                // Here, remove the 'd-none' class from the select tag
                const successForm = document.getElementById('create-attendee-form');
                successForm.classList.add('d-none');
            }
        } catch (e) {
            // Figure out what to do if an error is raised
            console.log(e);
            // console.error("There was an error", e);
        }

    });

    }

  });
