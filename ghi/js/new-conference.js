
// console.log('Hello world')


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        //console.log(data)


        // Get the select tag element by its id 'state'
        const selectTag = document.getElementById('location')

        // For each state in the states property of the data
        for (let location of data.locations) {
            // Create an 'option' element
            const option = document.createElement('option')
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = location.id
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = location.name
            // Append the option element as a child of the select tag
            selectTag.appendChild(option);

        }

        //Select the Form to place event listener
        const formTag = document.getElementById('create-conference-form');
        //create event listener
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            // console.log('need to submit the form data');
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            // console.log(json);
            try {
                const conferenceUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                    // console.log(newLocation);
                }
            } catch (e) {
                // Figure out what to do if an error is raised
                console.log(e);
                // console.error("There was an error", e);
            }

        });



    }
})
